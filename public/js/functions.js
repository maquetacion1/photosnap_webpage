(function( $ ) {
    $(document).ready(function (){
        let mainNavigation= document.getElementsByClassName("main-navigation");
        let menutoggle = document.getElementsByClassName("menu-toggle");
        let linkItems = document.getElementsByClassName("link-item");
        let main =  document.getElementsByClassName("main");
        let properties =  document.getElementsByClassName("properties");
        let stories =  document.getElementsByClassName("stories");
        let features =  document.getElementsByClassName("features");
        let footer =  document.getElementsByClassName("footer");
        let contain = [main, properties, stories, features, footer];
        let month =  document.getElementsByClassName("option-month");
        let priceMonth = document.getElementsByClassName("entry-price-month");
        let year =  document.getElementsByClassName("option-year");
        let priceYear = document.getElementsByClassName("entry-price-year");

        // Menu de navegacion
        $(document).on('click','.menu-toggle',function(event){
            event.preventDefault();
            // Cuando el menu se cierra
            if(mainNavigation[0].classList.contains("open")){
                menutoggle[0].innerHTML =
                '<img src="./assets/shared/mobile/menu.svg" alt="close menu button" />';
                mainNavigation[0].classList.remove("open");
                removeShadow();

            } // Cuando abrimos el menu
            else {
                menutoggle[0].innerHTML =
                '<img src="./assets/shared/mobile/close.svg" alt="close menu button" />';
                mainNavigation[0].style.display="grid";
                mainNavigation[0].className += " open";
                addShadow();
            }
        });

        $(document).on('click','.link-item',function(event){
            event.preventDefault();
            const item = document.getElementById(event.currentTarget.id)
    
            if(item.classList.contains("active")){
                return;
            }

            removeActiveClass(linkItems);
            item.className += " active";
        });

        function removeActiveClass(items){
            for (let item of items) {
                if(item.classList.contains("active")){
                    item.classList.remove("active");
                }
            };
        }

        $(document).on('click','.switch-button__checkbox',function(event){
            if(month[0].classList.contains("inactive")){
                removeInactiveClass(month);
                removeInactiveClass(priceMonth);
                addInactiveClass(year);
                addInactiveClass(priceYear);
            } else {
                removeInactiveClass(year);
                removeInactiveClass(priceYear);
                addInactiveClass(month);
                addInactiveClass(priceMonth);
            }
        })
        
        function addInactiveClass(items){
            for (let item of items) {
                item.className += " inactive";
            };
        }
        function removeInactiveClass(items){
            for (let item of items) {
                if(item.classList.contains("inactive")){
                    item.classList.remove("inactive");
                }
            };
        }

        function addShadow(){
            contain.forEach(element => {
                element[0].className += " shadow";
            });
        }

        function removeShadow(){
            contain.forEach(element => {
                element[0].classList.remove("shadow");
            });
        }
    })
})( jQuery );